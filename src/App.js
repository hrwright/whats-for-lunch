import React, { Component } from 'react';
import './App.css';
import Home from './views/home/home';
import data from './data/restaurants';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      view: 'home',
      currentRestaurant: {}
    };
    this.changeRestaurant = this.changeRestaurant.bind(this);
  }

  changeRestaurant() {
    const index = Math.floor(Math.random() * data.length);
    this.setState({ currentRestaurant: data[index] });
  }

  componentDidMount() {
    this.changeRestaurant();
  }

  getView() {
    switch(this.state.view) {
      case 'home': 
        return <Home
          current={this.state.currentRestaurant}
          changeRestaurant={this.changeRestaurant}
          />;
      default: 
        return <Home />;
    }
  }

  render() {
    return (
      <div className="App">
        <header>
          <h1>What's for Lunch?</h1>
        </header>
        <div>
          {this.getView()}
        </div>
      </div>
    );
  }
}

export default App;
