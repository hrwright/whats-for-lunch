import React from 'react'
import PropTypes from 'prop-types';

import './button.css';

const Button = props => {
  return <button type='button' onClick={props.handleClick} className={props.type}>
    {props.text}
  </button>;
};

Button.propTypes = {
  handleClick: PropTypes.func,
  text: PropTypes.string,
  type: PropTypes.string
};

Button.defaultProps = {
  handleClick: undefined,
  text: '',
  type: 'button'
};

export default Button;