import React from 'react'
import Button from '../../components/button/button';

const Home = props => {
  function calculatePrice() {
    let price = '';
    switch(props.current.price) {
      case 1:
        price = '$';
        break;
      case 2:
        price = '$';
        break;
      case 3:
        price = '$';
        break;
      case 4:
        price = '$';
        break;
      case 5:
        price = '$';
        break;
      default: 
        price = 'unknown';
        break;
    }
    return price;
  }
  return <div>
    <h3>{props.current.name}</h3>
    {/* <div>({props.current.type})</div> */}
    {/* <div>{calculatePrice()}</div> */}
    <Button 
      handleClick={props.changeRestaurant}
      text='New Restaurant'
      type='button' />
  </div>
}

export default Home;