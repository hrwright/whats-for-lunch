const types = require('../constants/types');

let places = [
  {
    name: "Harvest Restaurant",
    price: 0,
    type: types.bakery
  },
  {
    name: "Del Taco",
    price: 1,
    type: types.mexican
  },
  {
    name: "Panda Express",
    price: 1,
    type: types.chinese
  },
  {
    name: "Cafe Rio Mexican Grill",
    price: 0,
    type: types.mexican
  },
  {
    name: "Wendy's",
    price: 1,
    type: types.american
  },
  {
    name: "McDonald's",
    price: 1,
    type: types.american
  },
  {
    name: "Five Guys",
    price: 2,
    type: types.american
  },
  {
    name: "Summit Inn Pizza",
    price: 0,
    type: types.american
  },
  {
    name: "Braza Grill",
    price: 3,
    type: types.brazillian
  },
  {
    name: "Chick-fil-A",
    price: 1,
    type: types.american
  },
  {
    name: "Cafe Zupas",
    price: 2,
    type: types.american
  },
  {
    name: "Arby's",
    price: 1,
    type: types.american
  },
  {
    name: "Teriyaki Grill",
    price: 1,
    type: types.chinese
  },
  {
    name: "Slapfish",
    price: 2,
    type: types.seafood
  },
  {
    name: "Bona Vita Italian Bistro",
    price: 2,
    type: types.italian
  },
  {
    name: "Chipotle Mexican Grill",
    price: 1,
    type: types.mexican
  },
  {
    name: "Village Baker",
    price: 0,
    type: types.bakery
  },
  {
    name: "Costa Vida",
    price: 1,
    type: types.mexican
  },
  {
    name: "Subway",
    price: 1,
    type: types.sandwich
  },
  {
    name: "Popeye's",
    price: 0,
    type: types.american
  },
  {
    name: "Cubby's",
    price: 2,
    type: types.bakery
  },
  {
    name: "Mo' Bettahs Hawaiian Style",
    price: 2,
    type: types.hawaiian
  },
  {
    name: "Aubergine & Company",
    price: 2,
    type: types.mediterranean
  },
  {
    name: "JCW's",
    price: 2,
    type: types.american
  },
  {
    name: "Firehouse Subs",
    price: 1,
    type: types.sandwich
  },
  {
    name: "Pizza Studio",
    price: 1,
    type: types.american
  },
  {
    name: "Smashburger",
    price: 2,
    type: types.american
  },
  {
    name: "Blaze Pizza",
    price: 1,
    type: types.american
  },
  {
    name: "JDawgs",
    price: 1,
    type: types.american
  },
  {
    name: "Popeyes Louisiana Kitchen",
    price: 1,
    type: types.american
  },
  {
    name: "SLABpizza",
    price: 0,
    type: types.american
  },
  {
    name: "Carl's Jr.",
    price: 1,
    type: types.american
  },
  {
    name: "Tower Deli at Thanksgiving Point",
    price: 0,
    type: types.bakery
  },
  {
    name: "PDQ Restaurant",
    price: 1,
    type: types.american
  },
  {
    name: "Spicy Tai Express",
    price: 1,
    type: types.thai
  },
  {
    name: "Johnny Rockets",
    price: 2,
    type: types.american
  },
  {
    name: "Pieology Pizzeria Lehi",
    price: 1,
    type: types.american
  },
  {
    name: "Jersey Mike's Subs",
    price: 1,
    type: types.bakery
  },
  {
    name: "Taco Bell",
    price: 1,
    type: types.mexican
  },
  {
    name: "Pizza Hut Express",
    price: 1,
    type: types.american
  },
  {
    name: "Capriotti's Sandwich Shop",
    price: 0,
    type: types.bakery
  },
  {
    name: "Tsunami Restaurant & Sushi Bar",
    price: 2,
    type: types.japanese
  },
  {
    name: "Brick Canvas Cafe",
    price: 0,
    type: types.bakery
  },
  {
    name: "Cafe Trang Bistro Lehi",
    price: 0,
    type: types.vietnamese
  },
  {
    name: "R&R",
    price: 2,
    type: types.american
  },
  {
    name: "Rumbi Island Grill",
    price: 1,
    type: types.hawaiian
  },
  {
    name: "The Rising Bun",
    price: 2,
    type: types.chinese
  },
  {
    name: "Ramen Nation",
    price: 1,
    type: types.chinese
  },
  {
    name: "Tucanos Brazilian Grill",
    price: 3,
    type: types.brazillian
  }
];

module.exports = places;